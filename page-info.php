<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>

<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/info-icon') ?>
            <p>INFOGRAPHICS</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>


<main id="infograph">
    <section>
        <h2 class="green vertical-head">INFOGRAPHICS</h2>
        <div class="page-width">
            <h3 class="title-border">数字で見る</h3>
            <div>
                <?php
                $info_graphics = SCF::get('info_graphics');
                $counter = 1;
                foreach ($info_graphics as $value) :
                    $imgurl = wp_get_attachment_image_src($value['info_graphics_image'], 'full');
                ?>
                    <?php if ($counter == 1) echo '<div class="info-content box">' ?>
                    <div class="info-content__area">
                        <div class="info-content__area--wrap">
                            <img src="<?php echo $imgurl[0] ?>" alt="">
                            <p class="graphics-title"><?php echo $value['info_graphics_titile']; ?></p>
                            <?php if ($value['info_graphics_val'] && $value['info_graphics_unit']) : ?>
                                <p class="graphics-num js-count">
                                    <span class="counted js-num" data-num="<?= $value['info_graphics_val']; ?>">
                                        0
                                    </span>
                                    <span class="unit"><?php echo $value['info_graphics_unit']; ?></span>
                                </p>
                            <?php elseif ($value['info_ratio_b'] && $value['info_ratio_a']) : ?>
                                <p class="graphics-num js-count">
                                    <span class="counted js-num" data-num="<?= $value['info_ratio_b']; ?>">0</span>
                                </p>
                                <p class="graphics-num js-count">
                                    <span class="hiritu">：</span>
                                    <span class="counted ratio_a js-num" data-num="<?= $value['info_ratio_a']; ?>">0</span>
                                </p>

                            <?php endif; ?>
                            <?php if ($value['info_ annotation']) : ?>
                                <p class="graphics-info"><?= $value['info_ annotation']; ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if ($counter == 4) echo '</div>'  ?>
                    <?php $counter++; ?>
                    <?php if ($counter == 5) $counter = 1  ?>

                <?php endforeach; ?>
            </div>

        </div>
    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>

<?php get_footer(); ?>

<script>
    $(function() {
        $(window).on("load scroll", function() {
            $(".js-count").each(function() {
                var txtPos = $(this).offset().top;
                var scroll = $(window).scrollTop();
                var windowHeight = $(window).height();
                if (scroll > txtPos - windowHeight + windowHeight / 20) {
                    if ($(".js-num", this).attr("data-num").indexOf(".") > 1) {
                        var rounding = 1;
                    } else if ($(".js-num", this).attr("data-num").indexOf(".") > -1) {
                        var rounding = 2;
                    } else {
                        var rounding = 0;
                    }
                    $(".js-num", this).numerator({
                        easing: "linear", //カウントアップの動き
                        duration: 2000, //カウントアップの時間
                        toValue: $(".js-num", this).attr("data-num"), //カウントアップする数値
                        rounding: rounding, //小数点以下の桁数（初期値：0）
                    });
                }
            });
        });
    })
</script>