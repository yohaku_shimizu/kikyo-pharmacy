<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>

<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/day-icon') ?>
            <p>ONE DAY</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>


<main id="oneday">
    <section class="oneday-sec01">
        <h2 class="green vertical-head">ONE DAY</h2>
        <div class="page-width">
            <h3 class="title-border">調剤事業の一日</h3>
            <?php $image1 = get_post_meta(get_the_ID(), 'dispensing_image'); ?>
            <?php foreach ($image1 as $value) : ?>
                <img src="<?php echo wp_get_attachment_url($value) ?>" class="box" alt="調剤事業の一日">
            <?php endforeach; ?>
            <article class="under-content">
                <div class="under-content--area">
                    <div class="under-content--text">
                        <?php
                        $dispensing1 = SCF::get('dispensing_oneday');
                        foreach ($dispensing1 as $value) :
                        ?>
                            <div class="text-box <?php if ($counter % 2 == 0) {
                                                        echo "num1 box-l";
                                                    } else {
                                                        echo "num2 box-r";
                                                    } ?>">
                                <p class="text-box__title"><?php echo $value['dispensing_time']; ?></p>
                                <p class="text-box__text"><?php echo nl2br($value['dispensing_work']); ?></p>

                            </div>
                            <?php $counter++; ?>
                        <?php endforeach; ?>
                        <p class="hosoku">※一日の流れは一例です。</p>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section class="oneday-sec02">
        <div class="page-width">
            <h3 class="title-border">施設同行の1日</h3>
            <?php $image2 = get_post_meta(get_the_ID(), 'dispensing_facility'); ?>
            <?php foreach ($image2 as $value) : ?>
                <img src="<?php echo wp_get_attachment_url($value) ?>" class="box" alt="施設同行の一日">
            <?php endforeach; ?>
            <article class="under-content">
                <div class="under-content--area">
                    <div class="under-content--text">
                        <?php
                        $dispensing2 = SCF::get('dispensing_facility_oneday');
                        foreach ($dispensing2 as $value) :
                        ?>
                            <div class="text-box <?php if ($counter % 2 == 0) {
                                                        echo "num2 box-l";
                                                    } else {
                                                        echo "num1 box-r";
                                                    } ?>">
                                <p class="text-box__title"><?php echo $value['dispensing_facility_time']; ?></p>
                                <p class="text-box__text"><?php echo nl2br($value['dispensing_facility_works']); ?></p>

                            </div>
                            <?php $counter++; ?>
                        <?php endforeach; ?>
                        <p class="hosoku">※1日の流れは一例です。（モデル：山本薬剤師）</p>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section class="oneday-sec03">
        <div class="page-width">
            <h3 class="title-border">在宅業務×調剤事業の1日</h3>
            <?php $image3 = get_post_meta(get_the_ID(), 'dispensing_home'); ?>
            <?php foreach ($image3 as $value) : ?>
                <img src="<?php echo wp_get_attachment_url($value) ?>" class="box" alt="在宅業務の一日">
            <?php endforeach; ?>
            <article class="under-content">
                <div class="under-content--area">
                    <div class="under-content--text">
                        <?php
                        $dispensing3 = SCF::get('dispensing_home_oneday');
                        foreach ($dispensing3 as $value) :
                        ?>
                            <div class="text-box <?php if ($counter % 2 == 0) {
                                                        echo "num1 box-l";
                                                    } else {
                                                        echo "num2 box-r";
                                                    } ?>">
                                <p class="text-box__title"><?php echo $value['dispensing_home_time']; ?></p>
                                <p class="text-box__text"><?php echo nl2br($value['dispensing_home_schedule']); ?></p>

                            </div>
                            <?php $counter++; ?>
                        <?php endforeach; ?>
                        <p class="hosoku">※一日の流れは一例です。</p>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>

<?php get_footer(); ?>