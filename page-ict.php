<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/ict-icon') ?>
            <p>ICT</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>


<main id="ict">
    <section>
        <h2 class="vertical-head green">ICT</h2>
        <div class="page-width">
            <h3 class="title-border">ICTの活用</h3>
            <article class="under-content">
                <div class="under-content--area">
                    <img src="<?= get_template_directory_uri() ?>/images/ict/ict01.jpg" alt="ICTの活用画像">
                    <div class="under-content--text">
                        <div class="text-box">
                            <p class="text-box__title">最新機器の導入でスマート薬局を目指します</p>
                            <p class="text-box__text">
                                キキョウファーマシーグループでは調剤ミスゼロを目指し、レセコン・散剤の監査システムを導入。<br>ICT活用で調剤業務の効率化を図り、その分、服薬指導やヒアリングなどに専念することができます。また、患者様の情報を一元管理することで、医療や介護との連携がしやすくなります。
                            </p>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section>
        <div class="page-width">
            <div class="equipment">
                <h3 class="title-border">機器設備</h3>
                <?php
                $career_flow = SCF::get('equipment');
                foreach ($career_flow as $value) :
                ?>
                    <?php
                    $imgurl = wp_get_attachment_image_src($value['equipment_image'], 'full');
                    if ($imgurl) :
                    ?>

                        <div class="equipment__content">
                            <div class="equipment__content--left">
                                <img src="<?php echo $imgurl[0] ?>" alt="">
                            </div>
                            <div class="equipment__content--right">
                                <p class="title"><?php echo $value['equipment_name']; ?></p>
                                <p class="text"><?php echo nl2br($value['equipment_text']); ?></p>
                                <?php if ($value['equipment_url']) : ?>
                                    <a target="_blank" href="<?php echo $value['equipment_url']; ?>">製品情報<?php get_template_part('/images/svg/link-k-icon'); ?></a>
                                <?php elseif ($value['equipment_pdf']) : ?>
                                    <?php $image = wp_get_attachment_url($value['equipment_pdf'], 'full', true); ?>
                                    <a target="_blank" href="<?= $image; ?>">製品情報（PDF）</a>
                                <?php else : ?>
                                    <a href="##">なにも設定されていません</a>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php else : ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>

<?php get_footer(); ?>