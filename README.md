# クライアント名・プロジェクト名

| クライアント名   | 株式会社　キキョウファーマシーグループ                                    |
| :--------------- | :------------------------------------------------------------------------ |
| 顧客番号         | 0000                                                                      |
| デザイン担当者   | 宮治                                                                      |
| 初期構築         | 清水、鈴木                                                                |
| CMS              | WordPress                                                                 |
| ドメイン         | example.com                                                               |
| ドメイン管理     | ドメイン管理会社,ドメイン管理サービス名                                   |
| 本番サーバー     | 本番サーバー管理会社、サービス名                                          |
| 本番サーバー状態 | 状態報告                                                                  |
| テストサーバー   | http://yohakutest.xsrv.jp/kikyou-f/                                       |
| Bitbucket        | https://bitbucket.org/yohaku_shimizu/kikyo-pharmacy/src/master/           |
| NAS              | share > 制作中の案件 > 0000_clientname                                    |
| GoogleDrive      | https://drive.google.com/drive/x/x/folders/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX |

## 案件概要

株式会社　キキョウファーマシーグループ公式サイトのリニューアルです。

## 必須プラグイン

- Advanced Custom Fields (カスタムフィールド設定)
- Simple Custom Fields（カスタムフィールド設定）
- Contact Form 7 (お問い合わせフォーム)
- Custom Post Type UI (カスタム投稿設定)
- WP SVG images（SVG をメディアで表示します）

## 投稿

- NEWS
- 薬局一覧
- 求人情報

## お客様更新

- スライド
- 固定ページ（メインビジュアル）
- 注釈文
- FQA
- FLOW

## 注意事項

### セクションクラスの命名規則

## 修正などの対応記録

| 年月日   | 実施者 | 内容 |
| :------- | :----- | :--- |
| 2021/~~~ | ~~~    | ~~~  |
