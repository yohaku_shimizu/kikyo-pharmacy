<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/career-icon') ?>
            <p>CAREER PLAN</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>


<main id='career-plan'>
    <section>
        <h2 class="vertical-head green">CAREER PLAN</h2>

        <div class="page-width">
            <div class="training-system">
                <h3 class="title-border">研修制度</h3>

                <?php $careerimg1 = get_post_meta(get_the_ID(), 'training_image'); ?>
                <?php foreach ($careerimg1 as $value) : ?>
                    <img src="<?php echo wp_get_attachment_url($value) ?>" class="box" alt="研修画像">
                <?php endforeach; ?>

                <article class="under-content box-r">
                    <div class="under-content--area">
                        <div class="under-content--text">
                            <div class="text-box">
                                <p class="text-box__title"><?php echo get_field('training_title'); ?></p>
                                <p class="text-box__text"><?php echo nl2br(get_field('training_textarea')); ?></p>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section>
        <div class="page-width">
            <div class="training-flow">
                <h3 class="title-border">研修の流れ</h3>
                <?php
                $career_flow = SCF::get('training_flow');
                $career_counter = 0;
                foreach ($career_flow as $value) : ?>
                    <div class="training-flow__area box">
                        <?php if ($career_counter == 0) : ?>
                            <p class="training-flow__area--title active button"><span><?php echo $value['training_flow_title']; ?></span><span class="btn"></span></p>
                        <?php else : ?>
                            <p class="training-flow__area--title button"><span><?php echo $value['training_flow_title']; ?></span><span class="btn"></span></p>
                        <?php endif; ?>
                        <?php
                        $imgurl = wp_get_attachment_image_src($value['training_flow_image'], 'full');
                        ?>
                        <?php if ($career_counter == 0) : ?>
                            <div class="training-flow__area--box areabox">
                            <?php else : ?>
                                <div class="training-flow__area--box">
                                <?php endif; ?>
                                <img src="<?php echo $imgurl[0] ?>" alt="">
                                <article class="under-content">
                                    <div class="under-content--area">
                                        <div class="under-content--text">
                                            <div class="text-box">
                                                <p class="text-box__text"><?php echo nl2br($value['training_flow_text']); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                </div>
                            </div>
                            <?php $career_counter = 1; ?>
                        <?php endforeach; ?>
                    </div>
            </div>
    </section>

    <section>
        <div class="page-width">
            <div class="qualification">
                <h3 class="title-border">目指せる資格と取得率</h3>
                <div>
                    <?php
                    $career_flow = SCF::get('qualification');
                    $counter = 1;
                    foreach ($career_flow as $value) :
                        $imgurl = wp_get_attachment_image_src($value['qualification_image'], 'full');
                    ?>
                        <?php if ($imgurl) : ?>
                            <?php if ($counter == 1) echo '<div class="info-content">' ?>
                            <div class="info-content__area box">
                                <div class="info-content__area--wrap">
                                    <img src="<?php echo $imgurl[0] ?>" alt="">
                                    <p class="graphics-title"><?php echo $value['qualification_name']; ?></p>
                                    <p class="graphics-year js-counte">
                                        <span class="counted shuffle">
                                            <?php echo $value['qualification_year']; ?>
                                        </span>
                                        <span class="unit">名</span>
                                    </p>
                                    <?php if ($value['qualification_info']) : ?>
                                        <p class="graphics-info"><?= $value['qualification_info']; ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php if ($counter == 4) echo '</div>'  ?>
                            <?php $counter++; ?>
                            <?php if ($counter == 5) $counter = 1  ?>
                        <?php else : ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
                <p class="annotation">※資格取得は任意です。</p>
            </div>
        </div>
    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>

<?php get_footer(); ?>

<script>
    $(function() {
        $('.training-flow__area--title').on('click', function() {
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });
    });
    $(function() {
        var animationFlag = false;
        $(window).on("load scroll", function() {

            $(".js-counte").each(function() {
                var txtPos = $(this).offset().top;
                var scroll = $(window).scrollTop();
                var windowHeight = $(window).height();
                if (scroll > txtPos - windowHeight + windowHeight / 20) {
                    if (!animationFlag) {
                        animationFlag = true;
                        var shuffleElm = $('.shuffle'),
                            shuffleSpeed = 40,
                            shuffleAnimation = 2000,
                            shuffleDelay = 1000;
                        shuffleElm.each(function(t) {
                            var self = $(this);
                            self.wrapInner('<span class="shuffleWrap"></span>');
                            var shuffleWrap = self.find('.shuffleWrap');
                            shuffleWrap.replaceWith(shuffleWrap.text().replace(/(\S)/g, '<span class="shuffleNum">$&</span>'));
                            var shuffleNum = self.find('.shuffleNum'),
                                numLength = shuffleNum.length;
                            shuffleNum.each(function(i) {
                                var selfNum = $(this),
                                    thisNum = selfNum.text(),
                                    shuffleTimer;

                                function timer() {
                                    shuffleTimer = setInterval(function() {
                                        rdm = Math.floor(Math.random() * (9)) + 1;
                                        selfNum.text(rdm);
                                    }, shuffleSpeed);
                                }
                                timer();
                                var i = -i + numLength;
                                setTimeout(function() {
                                    clearInterval(shuffleTimer);
                                    selfNum.text(thisNum);
                                }, shuffleAnimation + (i * shuffleDelay));
                            });
                            self.css({
                                visibility: 'visible'
                            });
                        });
                    }
                }
            });
        });
    })
</script>