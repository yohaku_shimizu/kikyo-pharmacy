<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/recruit_under') ?>
            <p>RECRUIT</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>


<main id="single-recruit">
    <section>
        <h2 class="green vertical-head">RECRUIT</h2>
        <div class="page-width">
            <h3 class="title-border"><?php the_title(); ?>の求人情報</h3>
            <?php
            $args = array(
                'post_type' => 'recruit',
                'posts_per_page' => -1,
                'meta_query' => array(
                    array(
                        'key' => 'pharmacy',
                        'value' => '"' . get_the_ID() . '"',
                        'compare' => 'LIKE'
                    )
                )
            );
            $the_query = new WP_Query($args);
            if ($the_query->have_posts()) :
            ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="pharmacy-area">
                        <?php $reccomendItem = get_field('pharmacy'); ?>
                        <?php if ($reccomendItem) : ?>
                            <?php foreach ($reccomendItem as $key => $value) : ?>
                                <img src="<?= get_field('pharmacy_image', $value->ID) ?>" alt="">
                                <?php $address = get_field('pharmacy_addres', $value->ID); ?>
                                <div class="pharmacy-area__right">
                                    <p class="pharmacy-info">店舗情報</p>
                                    <p><?= get_field('pharmacy_addresnum', $value->ID) ?></p>
                                    <p><?= $address; ?></p>
                                    <p>TEL <?= get_field('pharmacy_phone2', $value->ID) ?></p>
                                    <p>夜間等緊急連絡先 <?= get_field('pharmacy_phone', $value->ID) ?></p>
                                    <p>FAX <?= get_field('pharmacy_fax', $value->ID) ?></p>
                                    <p>営業時間 <?= nl2br(get_field('pharmacy_time', $value->ID)) ?></p>
                                    <p>定休日<?= get_field('pharmacy_holiday', $value->ID) ?></p>
                                    <div class="plus">
                                        <p class="title-s-border"><a class="google-map" target="_blank" href="https://www.google.co.jp/maps/place/<?php echo $address; ?>"><?php get_template_part('/images/svg/google-map'); ?>Google Map<?php get_template_part('/images/svg/link-k-icon'); ?></a></p>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
            <?php
            $career_flow = SCF::get('job_description');
            $counter = 0;
            foreach ($career_flow as $value) :
            ?>
                <?php if ($value['job_description_name']) :  ?>
                    <div class="recruit-info">
                        <?php if ($value['job_description_name']) :  ?>
                            <div class="recruit-info__title button">
                                <h3><?php echo $value['job_description_name']; ?></h3>
                                <span class="btn"></span>
                            </div>
                        <?php endif; ?>
                        <?php if ($value['job_description_paying']) :  ?>
                            <div class="recruit-info__area ac-child">
                                <div class="recruit-info__area--item">
                                    <p class="item-title">給与</p>
                                    <p><?php echo $value['job_description_paying']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_occupation']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">募集職種</p>
                                    <p><?php echo $value['job_description_occupation']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_necessary']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">応募資格</p>
                                    <p><?php echo $value['job_description_necessary']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_status']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">雇用形態</p>
                                    <p><?php echo $value['job_description_status']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_job']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">業務内容</p>
                                    <p><?php echo $value['job_description_job']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_address']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">勤務地</p>
                                    <p><?php echo $value['job_description_address']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_time']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">勤務時間</p>
                                    <p><?php echo $value['job_description_time']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_holiday']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">休日</p>
                                    <p><?php echo $value['job_description_holiday']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_off']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">休暇</p>
                                    <p><?php echo $value['job_description_off']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_bonus']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">賞与</p>
                                    <p><?php echo $value['job_description_bonus']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_raise']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">昇給</p>
                                    <p><?php echo $value['job_description_raise']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_hukuri']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">福利厚生</p>
                                    <p><?php echo nl2br($value['job_description_hukuri']); ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_expenses']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">交通費</p>
                                    <p><?php echo $value['job_description_expenses']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_age']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">定年</p>
                                    <p><?php echo $value['job_description_age']; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ($value['job_description_flow']) :  ?>
                                <div class="recruit-info__area--item">
                                    <p class="item-title">選考フロー</p>
                                    <p><?php echo nl2br($value['job_description_flow']); ?></p>
                                </div>
                            <?php endif; ?>
                            <div class="recruit-info__area--buttons">
                                <!-- <a class="recruit-button recruit-button-l" href="##">職場見学予約<?php get_template_part('/images/svg/link-k-icon'); ?></a> -->
                                <a class="recruit-button recruit-button-r" href="<?= home_url() ?>/entry?pharmacy=<?php the_title(); ?>&type=<?php echo $value['job_description_name']; ?>">エントリー</a>
                            </div>
                            </div>
                    </div>
                <?php else : ?>
                    <?php $counter++;
                    if ($counter >= 6) {
                        echo "<p>現在、求人情報はありません。</p>";
                        break;
                    }
                    ?>
                <?php endif; ?>

            <?php endforeach; ?>


        </div>
    </section>


    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>

<?php get_footer(); ?>
<script>
    $(function() {
        $('.recruit-info__title').on('click', function() {
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });
    });
</script>