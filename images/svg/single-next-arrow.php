<svg xmlns="http://www.w3.org/2000/svg" width="6.121" height="9.414" viewBox="0 0 6.121 9.414">
    <defs>
        <style>
            .single-next-arrow {
                fill: none;
                stroke: #3e366b;
                stroke-miterlimit: 10;
                stroke-width: 2px;
            }
        </style>
    </defs>
    <path id="next-arrow" class="single-next-arrow" d="M22,28l4-4-4-4" transform="translate(-21.293 -19.293)" />
</svg>