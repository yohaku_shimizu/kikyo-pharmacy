<svg class="link-icon" xmlns="http://www.w3.org/2000/svg" width="15" height="13.133" viewBox="0 0 15 13.133">
    <defs>
        <style>
            .link-k-1 {
                fill: none;
                stroke: #3e366b;
                transition: 0.3s;
            }
        </style>
    </defs>
    <g id="link-k" transform="translate(-33.596 -32.539)">
        <rect id="sikaku-k" class="link-k-1" width="11.2" height="9.333" transform="translate(48.096 42.372) rotate(180)" />
        <path id="path-k" class="link-k-1" d="M34.1,35.839v9.333H45.3" />
    </g>
</svg>