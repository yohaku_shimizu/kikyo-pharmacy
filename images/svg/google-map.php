<svg class="google-map-icon" xmlns="http://www.w3.org/2000/svg" width="12" height="17.143" viewBox="0 0 12 17.143">
    <defs>
        <style>
            .google-1 {
                fill: #3e366b;
            }
        </style>
    </defs>
    <path id="map_icon" class="google-1" d="M714.687,232.875a6,6,0,0,0-6,6c0,4.5,6,11.143,6,11.143s6-6.643,6-11.143A6,6,0,0,0,714.687,232.875Zm0,8.143a2.143,2.143,0,1,1,2.143-2.143A2.143,2.143,0,0,1,714.687,241.018Z" transform="translate(-708.687 -232.875)" />
</svg>