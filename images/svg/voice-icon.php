<svg xmlns="http://www.w3.org/2000/svg" width="35.999" height="48" viewBox="0 0 35.999 48">
    <defs>
        <style>
            .voice-1 {
                fill: none;
                stroke-miterlimit: 10;
                stroke-width: 4px;
            }
        </style>
    </defs>
    <g id="icon_voice" transform="translate(-363.275 -83)">
        <path id="パス_27136" data-name="パス 27136" class="voice-1 cstm-stroke" d="M396.774,106.068a16,16,0,1,1-32,0" transform="translate(0.5)" />
        <rect id="長方形_1820" data-name="長方形 1820" class="cstm-fill" width="14" height="30" rx="6.524" transform="translate(373.775 83)" />
        <rect id="長方形_1821" data-name="長方形 1821" class="cstm-fill" width="4" height="10" transform="translate(378.775 121)" />
    </g>
</svg>