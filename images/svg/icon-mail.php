<svg xmlns="http://www.w3.org/2000/svg" width="48.17" height="36" viewBox="0 0 48.17 36">
    <defs>
        <style>
            .mail-icon-1,
            .mail-icon-3 {
                fill: none;
            }

            .mail-icon-1 {
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 4px;
            }

            .mail-icon-2 {
                stroke: none;
            }
        </style>
    </defs>
    <g id="icon_mail" transform="translate(0.085 -6)">
        <g id="長方形_1882" data-name="長方形 1882" class="mail-icon-1" transform="translate(0 6)">
            <rect class="mail-icon-2" width="48" height="36" />
            <rect class="mail-icon-3" x="2" y="2" width="44" height="32" />
        </g>
        <path id="パス_28637" data-name="パス 28637" class="mail-icon-1" d="M1,12.146,24,27,47,12.146" />
    </g>
</svg>