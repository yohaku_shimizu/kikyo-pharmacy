<svg xmlns="http://www.w3.org/2000/svg" width="44" height="48" viewBox="0 0 44 48">
    <defs>
        <style>
            .service-2,
            .service-4 {
                fill: none;
            }

            .service-2 {
                stroke-width: 4px;
            }

            .service-3 {
                stroke: none;
            }
        </style>
    </defs>
    <g id="icon_service" transform="translate(-555 -1251)">
        <rect id="長方形_1810" data-name="長方形 1810" class="cstm-fill" width="24" height="4" transform="translate(565 1251)" />
        <g id="長方形_1811" data-name="長方形 1811" class="service-2 cstm-stroke" transform="translate(555 1259)">
            <rect class="service-3" width="44" height="40" />
            <rect class="service-4" x="2" y="2" width="40" height="36" />
        </g>
        <rect id="長方形_1812" data-name="長方形 1812" class="cstm-fill" width="20" height="4" transform="translate(567 1277)" />
        <rect id="長方形_1813" data-name="長方形 1813" class="cstm-fill" width="20" height="4" transform="translate(579 1269) rotate(90)" />
    </g>
</svg>