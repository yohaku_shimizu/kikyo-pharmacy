<svg id="icon_career" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
    <defs>
        <style>
            .career-1,
            .career-4 {
                fill: none;
            }

            .career-1 {
                stroke-miterlimit: 10;
                stroke-width: 4px;
            }


            .career-3 {
                stroke: none;
            }
        </style>
    </defs>
    <g id="長方形_1822" data-name="長方形 1822" class="career-1 cstm-stroke">
        <rect class="career-3" width="48" height="35" />
        <rect class="career-4" x="2" y="2" width="44" height="31" />
    </g>
    <rect id="長方形_1823" data-name="長方形 1823" class="cstm-fill" width="48" height="8" />
    <path id="パス_27147" data-name="パス 27147" class="career-1 cstm-stroke" d="M10.386,26.1l7.465-7.465,7.9,4.9L37.614,13" />
    <g id="グループ_4410" data-name="グループ 4410">
        <path id="パス_27148" data-name="パス 27148" class="cstm-fill" d="M17.244,35,9.739,48h4.618l7.505-13Z" />
        <path id="パス_27149" data-name="パス 27149" class="cstm-fill" d="M26.137,35l7.505,13h4.618L30.756,35Z" />
    </g>
</svg>