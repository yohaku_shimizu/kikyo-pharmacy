<svg id="icon_pharmacy" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
    <defs>
        <style>
            .pharmacy-under-1,
            .pharmacy-under-5 {
                fill: none;
            }

            .pharmacy-under-1 {
                stroke: #fff;
                stroke-miterlimit: 10;
                stroke-width: 4px;
            }

            .pharmacy-under-2 {
                fill: #fff;
            }

            .pharmacy-under-3 {
                fill: #3e366b;
            }

            .pharmacy-under-4 {
                stroke: none;
            }
        </style>
    </defs>
    <g id="長方形_1835" data-name="長方形 1835" class="pharmacy-under-1" transform="translate(0 8)">
        <rect class="pharmacy-under-4" width="48" height="40" />
        <rect class="pharmacy-under-5" x="2" y="2" width="44" height="36" />
    </g>
    <rect id="長方形_1836" data-name="長方形 1836" class="pharmacy-under-2" width="48" height="19" />
    <g id="長方形_1837" data-name="長方形 1837" class="pharmacy-under-1" transform="translate(13 26)">
        <rect class="pharmacy-under-4" width="22" height="22" />
        <rect class="pharmacy-under-5" x="2" y="2" width="18" height="18" />
    </g>
    <g id="グループ_4421" data-name="グループ 4421">
        <rect id="長方形_1838" data-name="長方形 1838" class="pharmacy-under-3" width="9" height="2.571" transform="translate(33 8.214)" />
        <rect id="長方形_1839" data-name="長方形 1839" class="pharmacy-under-3" width="9" height="2.571" transform="translate(38.786 5) rotate(90)" />
    </g>
    <rect id="長方形_1840" data-name="長方形 1840" class="pharmacy-under-2" width="4" height="22" transform="translate(22 26)" />
</svg>