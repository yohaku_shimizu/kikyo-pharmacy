<svg id="icon_news" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
    <defs>
        <style>
            .news-under-1 {
                fill: none;
                stroke-miterlimit: 10;
            }

            .news-under-2,
            .news-under-3 {
                stroke: none;
            }

            .news-under-3 {
                fill: #fff;
            }
        </style>
    </defs>
    <g id="パス_27170" data-name="パス 27170" class="news-under-1">
        <path class="news-under-2" d="M48,0,19.5,9.5H0v24H19.5L48,43Z" />
        <path class="news-under-3" d="M 44 5.549705505371094 L 20.14911651611328 13.5 L 4 13.5 L 4 29.5 L 20.14911651611328 29.5 L 44 37.45029449462891 L 44 5.549705505371094 M 48 0 L 48 43 L 19.5 33.5 L 0 33.5 L 0 9.5 L 19.5 9.5 L 48 0 Z" />
    </g>
    <g id="パス_27171" data-name="パス 27171" class="news-under-1">
        <path class="news-under-2" d="M22,48H14L4,32h8Z" />
        <path class="news-under-3" d="M 22 48 L 14 48 L 4 32 L 12 32 L 22 48 Z" />
    </g>
</svg>