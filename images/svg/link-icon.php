<svg class="link-icon" xmlns="http://www.w3.org/2000/svg" width="15" height="13.133" viewBox="0 0 15 13.133">
    <defs>
        <style>
            .clsa-1 {
                fill: none;
                stroke: #fff;
                transition: 0.3s;
            }
        </style>
    </defs>
    <g id="link" transform="translate(-33.596 -32.539)">
        <rect id="長方形_662" data-name="長方形 662" class="clsa-1" width="11.2" height="9.333" transform="translate(48.096 42.372) rotate(180)" />
        <path id="パス_1890" data-name="パス 1890" class="clsa-1" d="M34.1,35.839v9.333H45.3" />
    </g>
</svg>