<svg xmlns="http://www.w3.org/2000/svg" width="249" height="52.6" viewBox="0 0 249 52.6">
    <defs>
        <style>
            .footer-1,
            .footer-2 {
                fill: #3e366b;
            }

            .footer-1 {
                font-size: 24px;
                font-family: Kan412TyposStd-Regular, Kan412Typos Std;
                letter-spacing: 0.04em;
            }

            .footer-2 {
                stroke: #3e366b;
                stroke-width: 0.6px;
                font-size: 12px;
                font-family: CezanneProN-M, FOT-Cezanne ProN;
                letter-spacing: 0.6em;
            }
        </style>
    </defs>
    <g id="logo" transform="translate(-115 -215.11)">
        <g id="グループ_4436" data-name="グループ 4436" transform="translate(110.703 228.011)">
            <text id="キキョウファーマシー" class="footer-1" transform="translate(4.297 19.099)">
                <tspan x="0" y="0">キキョウファーマシー</tspan>
            </text>
            <text id="GROUP" class="footer-2" transform="translate(93.797 37.099)">
                <tspan x="0" y="0">GROUP</tspan>
            </text>
        </g>
    </g>
</svg>