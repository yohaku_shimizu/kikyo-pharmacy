<svg xmlns="http://www.w3.org/2000/svg" width="40" height="48" viewBox="0 0 40 48">
    <defs>
        <style>
            .recruit-1 {
                transition: 0.3s;
            }
        </style>
    </defs>
    <g id="icon_recruit" transform="translate(-4)">
        <g id="グループ_4415" data-name="グループ 4415">
            <rect id="長方形_1828" data-name="長方形 1828" class="recruit-1 cstm-fill" width="24" height="4" transform="translate(12 27)" />
            <rect id="長方形_1829" data-name="長方形 1829" class="recruit-1 cstm-fill" width="24" height="4" transform="translate(12 35)" />
        </g>
        <g id="グループ_4416" data-name="グループ 4416">
            <path id="パス_27155" data-name="パス 27155" class="recruit-1 cstm-fill" d="M32,12h8V8L36,4H32Z" />
            <path id="パス_27156" data-name="パス 27156" class="recruit-1 cstm-fill" d="M40,12V44H8V4H32V0H4V48H44V12Z" />
            <path id="パス_27157" data-name="パス 27157" class="recruit-1 cstm-fill" d="M44,12,40,8v4Z" />
            <path id="パス_27158" data-name="パス 27158" class="recruit-1 cstm-fill" d="M36,4,32,0V4Z" />
        </g>
    </g>
</svg>