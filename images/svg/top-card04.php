<svg id="icon_entry" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
    <defs>
        <style>
            .card04-1 {
                fill: #3e366b;
            }
        </style>
    </defs>
    <rect id="長方形_1834" data-name="長方形 1834" class="card04-1" width="24" height="6" transform="translate(0 21)" />
    <path id="パス_558" data-name="パス 558" class="card04-1" d="M34,24,22,17.069V30.931Z" />
    <path id="パス_27160" data-name="パス 27160" class="card04-1" d="M8,0V14h4V4H44V44H12V34H8V48H48V0Z" />
</svg>