<svg preserveAspectRatio="xMinYMin slice" object-fit id="hexa-anime1" class="hexa-anime option-highspeed" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1600" height="900" viewBox="0 0 1600 900">
    <defs>
        <style>
            .cls-1 {
                fill: #d9d8d8;
                opacity: 0.2;
            }

            .cls-2 {
                clip-path: url(#clip-path);
            }

            .cls-3 {
                opacity: 0.65;
            }

            .cls-4 {
                fill: #fff;
            }

            .cls-5 {
                fill: #e4f4ee;
            }
        </style>
        <clipPath id="clip-path">
            <rect id="長方形_1841" data-name="長方形 1841" class="cls-1" width="1600" height="900" />
        </clipPath>
    </defs>
    <g id="マスクグループ_3" data-name="マスクグループ 3" class="cls-2">
        <g id="_6角_65_" data-name="6角_65%" class="cls-3" transform="translate(0 -42)">
            <path id="多角形_2" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(216 96) rotate(90)" />
            <path id="多角形_2-2" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(180 158) rotate(90)" />
            <path id="多角形_2-3" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440 220) rotate(90)" />
            <path id="多角形_2-4" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1584 220) rotate(90)" />
            <path id="多角形_2-5" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1656 220) rotate(90)" />
            <path id="多角形_2-6" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1476 282) rotate(90)" />
            <path id="多角形_2-7" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1548 282) rotate(90)" />
            <path id="多角形_2-8" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(180 406) rotate(90)" />
            <path id="多角形_2-9" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(72 468) rotate(90)" />
            <path id="多角形_2-10" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(144 468) rotate(90)" />
            <path id="多角形_2-11" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(288 468) rotate(90)" />
            <path id="多角形_2-12" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440 468) rotate(90)" />
            <path id="多角形_2-13" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(36 530) rotate(90)" />
            <path id="多角形_2-14" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(108 530) rotate(90)" />
            <path id="多角形_2-15" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(252 530) rotate(90)" />
            <path id="多角形_2-16" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(324 530) rotate(90)" />
            <path id="多角形_2-17" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(468 530) rotate(90)" />
            <path id="多角形_2-18" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1332 530) rotate(90)" />
            <path id="多角形_2-19" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1476 530) rotate(90)" />
            <path id="多角形_2-20" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1548 530) rotate(90)" />
            <path id="多角形_2-21" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1620 530) rotate(90)" />
            <path id="多角形_2-22" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(72 592) rotate(90)" />
            <path id="多角形_2-23" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(144 592) rotate(90)" />
            <path id="多角形_2-24" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(360 592) rotate(90)" />
            <path id="多角形_2-25" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(432 592) rotate(90)" />
            <path id="多角形_2-26" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1008 592) rotate(90)" />
            <path id="多角形_2-27" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1224 592) rotate(90)" />
            <path id="多角形_2-28" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1296 592) rotate(90)" />
            <path id="多角形_2-29" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1368 592) rotate(90)" />
            <path id="多角形_2-30" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440 592) rotate(90)" />
            <path id="多角形_2-31" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(108 654) rotate(90)" />
            <path id="多角形_2-32" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(180 654) rotate(90)" />
            <path id="多角形_2-33" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(252 654) rotate(90)" />
            <path id="多角形_2-34" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(324 654) rotate(90)" />
            <path id="多角形_2-35" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1044 654) rotate(90)" />
            <path id="多角形_2-36" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1116 654) rotate(90)" />
            <path id="多角形_2-37" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1188 654) rotate(90)" />
            <path id="多角形_2-38" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1332 654) rotate(90)" />
            <path id="多角形_2-39" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1404 654) rotate(90)" />
            <path id="多角形_2-40" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1476 654) rotate(90)" />
            <path id="多角形_2-41" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1620 654) rotate(90)" />
            <path id="多角形_2-42" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(216 716) rotate(90)" />
            <path id="多角形_2-43" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(360 716) rotate(90)" />
            <path id="多角形_2-44" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(936 716) rotate(90)" />
            <path id="多角形_2-45" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1224 716) rotate(90)" />
            <path id="多角形_2-46" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1512 716) rotate(90)" />
            <path id="多角形_2-47" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1584 716) rotate(90)" />
            <path id="多角形_2-48" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1656 716) rotate(90)" />
            <path id="多角形_2-49" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(684 778) rotate(90)" />
            <path id="多角形_2-50" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(756 778) rotate(90)" />
            <path id="多角形_2-51" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1116 778) rotate(90)" />
            <path id="多角形_2-52" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1188 778) rotate(90)" />
            <path id="多角形_2-53" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1260 778) rotate(90)" />
            <path id="多角形_2-54" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1332 778) rotate(90)" />
            <path id="多角形_2-55" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1476 778) rotate(90)" />
            <path id="多角形_2-56" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1548 778) rotate(90)" />
            <path id="多角形_2-57" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1296 840) rotate(90)" />
            <path id="多角形_2-58" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1368 840) rotate(90)" />
            <path id="多角形_2-59" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440 840) rotate(90)" />
            <path id="多角形_2-60" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1584 840) rotate(90)" />
            <path id="多角形_2-61" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1044 902) rotate(90)" />
            <path id="多角形_2-62" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1188 902) rotate(90)" />
            <path id="多角形_2-63" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1260 902) rotate(90)" />
            <path id="多角形_2-64" data-name="多角形 2" class="cls-5" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1548 902) rotate(90)" />
            <path id="多角形_2-65" data-name="多角形 2" class="cls-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1620 902) rotate(90)" />
        </g>
    </g>
</svg>