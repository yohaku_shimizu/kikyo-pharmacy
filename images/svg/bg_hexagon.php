<svg preserveAspectRatio="xMinYMin slice" object-fit id="hexa-anime3" class="hexa-anime option-highspeed" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1600" height="367" viewBox="0 0 1600 367">
    <defs>
        <style>
            .clsss-1,
            .clsss-3 {
                fill: #fff;
            }



            .clsss-1 {
                stroke: #707070;
            }

            .clsss-2 {
                clip-path: url(#clip-patha);
            }
        </style>
        <clipPath id="clip-patha">
            <rect id="_18" data-name="18" class="clsss-1" width="1600" height="367" transform="translate(0 2902)" />
        </clipPath>
    </defs>
    <g id="bg_Hex12" class="clsss-2" transform="translate(0 -2902)">
        <g id="rokaku" transform="translate(-10 2434)">
            <path id="多角形_2" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(288 468) rotate(90)" />
            <path id="多角形_2-2" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440 468) rotate(90)" />
            <path id="多角形_2-3" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(252 530) rotate(90)" />
            <path id="多角形_2-4" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(468 530) rotate(90)" />
            <path id="多角形_2-5" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1332 530) rotate(90)" />
            <path id="多角形_2-6" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1476 530) rotate(90)" />
            <path id="多角形_2-7" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1620 530) rotate(90)" />
            <path id="多角形_2-8" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(72 592) rotate(90)" />
            <path id="多角形_2-9" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(360 592) rotate(90)" />
            <path id="多角形_2-10" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(432 592) rotate(90)" />
            <path id="多角形_2-11" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1224 592) rotate(90)" />
            <path id="多角形_2-12" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1368 592) rotate(90)" />
            <path id="多角形_2-13" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440 592) rotate(90)" />
            <path id="多角形_2-14" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(180 654) rotate(90)" />
            <path id="多角形_2-15" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1116 654) rotate(90)" />
            <path id="多角形_2-16" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1188 654) rotate(90)" />
            <path id="多角形_2-17" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1332 654) rotate(90)" />
            <path id="多角形_2-18" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1404 654) rotate(90)" />
            <path id="多角形_2-19" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1620 654) rotate(90)" />
            <path id="多角形_2-20" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(360 716) rotate(90)" />
            <path id="多角形_2-21" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(936 716) rotate(90)" />
            <path id="多角形_2-22" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1224 716) rotate(90)" />
            <path id="多角形_2-23" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1296 716) rotate(90)" />
            <path id="多角形_2-24" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440 716) rotate(90)" />
            <path id="多角形_2-25" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1512 716) rotate(90)" />
            <path id="多角形_2-26" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1584 716) rotate(90)" />
            <path id="多角形_2-27" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1656 716) rotate(90)" />
            <path id="多角形_2-28" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(684 778) rotate(90)" />
            <path id="多角形_2-29" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1116 778) rotate(90)" />
            <path id="多角形_2-30" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1188 778) rotate(90)" />
            <path id="多角形_2-31" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1332 778) rotate(90)" />
            <path id="多角形_2-32" data-name="多角形 2" class="clsss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1404 778) rotate(90)" />
            <path id="多角形_2-33" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1476 778) rotate(90)" />
            <path id="多角形_2-34" data-name="多角形 2" class="clsss-3" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1548 778) rotate(90)" />
        </g>
    </g>
</svg>