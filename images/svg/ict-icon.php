<svg xmlns="http://www.w3.org/2000/svg" width="47.258" height="48" viewBox="0 0 47.258 48">
    <defs>
        <style>

        </style>
    </defs>
    <g id="icon_ICT" transform="translate(-631.185 -151.151)">
        <rect id="長方形_1817" data-name="長方形 1817" class="cstm-fill" width="40.749" height="8.15" transform="translate(631.185 191.001)" />
        <circle id="楕円形_6449" data-name="楕円形 6449" class="cstm-fill" cx="6.112" cy="6.112" r="6.112" transform="translate(633.222 157.893)" />
        <rect id="長方形_1818" data-name="長方形 1818" class="cstm-fill" width="17.318" height="4.075" transform="translate(643.409 161.968)" />
        <rect id="長方形_1819" data-name="長方形 1819" class="cstm-fill" width="12.225" height="4.075" transform="translate(660.728 157.893) rotate(90)" />
        <g id="グループ_4401" data-name="グループ 4401">
            <path id="パス_27131" data-name="パス 27131" class="cstm-fill" d="M659.419,164.848l-3.312-2.375,8.12-11.322,14.216,7.947-1.989,3.557-11.037-6.171Z" />
            <path id="パス_27132" data-name="パス 27132" class="cstm-fill" d="M664.227,176.859l-8.12-11.322,3.312-2.375,6,8.364,11.037-6.171,1.989,3.557Z" />
        </g>
        <path id="パス_27133" data-name="パス 27133" class="cstm-fill" d="M640.79,160.191H637.3v38.621H650.54Z" />
    </g>
</svg>