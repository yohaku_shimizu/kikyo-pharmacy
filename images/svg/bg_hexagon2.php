<svg preserveAspectRatio="xMinYMin slice" object-fit id="hexa-anime2" class="hexa-anime" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1600" height="310" viewBox="0 0 1600 310">
    <defs>
        <style>
            .clss-1 {
                fill: #fff;
                stroke: #707070;
            }

            .clss-2 {
                clip-path: url(#clip-paths);
            }

            .clss-3 {
                opacity: 0.66;
            }

            .clss-4 {
                fill: #3e366b !important;
            }
        </style>
        <clipPath id="clip-paths">
            <rect id="sikaku" class="clss-1" width="1600" height="310" transform="translate(0 2902)" />
        </clipPath>
    </defs>
    <g id="bg__Hex" data-name="bg_ Hex" class="clss-2" transform="translate(0 -2902)">
        <g id="six" class="clss-3" transform="translate(-10 2902)">
            <path id="多角形_374" data-name="多角形 374" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1440) rotate(90)" />
            <path id="多角形_390" data-name="多角形 390" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(288) rotate(90)" />
            <path id="多角形_397" data-name="多角形 397" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1476 62) rotate(90)" />
            <path id="多角形_400" data-name="多角形 400" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1260 62) rotate(90)" />
            <path id="多角形_412" data-name="多角形 412" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(396 62) rotate(90)" />
            <path id="多角形_414" data-name="多角形 414" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(252 62) rotate(90)" />
            <path id="多角形_416" data-name="多角形 416" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(108 62) rotate(90)" />
            <path id="多角形_417" data-name="多角形 417" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1656 124) rotate(90)" />
            <path id="多角形_421" data-name="多角形 421" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1368 124) rotate(90)" />
            <path id="多角形_422" data-name="多角形 422" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1296 124) rotate(90)" />
            <path id="多角形_433" data-name="多角形 433" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(504 124) rotate(90)" />
            <path id="多角形_435" data-name="多角形 435" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(360 124) rotate(90)" />
            <path id="多角形_436" data-name="多角形 436" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(288 124) rotate(90)" />
            <path id="多角形_442" data-name="多角形 442" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1548 186) rotate(90)" />
            <path id="多角形_455" data-name="多角形 455" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(612 186) rotate(90)" />
            <path id="多角形_456" data-name="多角形 456" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(540 186) rotate(90)" />
            <path id="多角形_458" data-name="多角形 458" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(396 186) rotate(90)" />
            <path id="多角形_459" data-name="多角形 459" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(324 186) rotate(90)" />
            <path id="多角形_462" data-name="多角形 462" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(108 186) rotate(90)" />
            <path id="多角形_467" data-name="多角形 467" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1368 248) rotate(90)" />
            <path id="多角形_476" data-name="多角形 476" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(720 248) rotate(90)" />
            <path id="多角形_479" data-name="多角形 479" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(504 248) rotate(90)" />
            <path id="多角形_480" data-name="多角形 480" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(432 248) rotate(90)" />
            <path id="多角形_481" data-name="多角形 481" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(360 248) rotate(90)" />
            <path id="多角形_482" data-name="多角形 482" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(288 248) rotate(90)" />
            <path id="多角形_483" data-name="多角形 483" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(216 248) rotate(90)" />
            <path id="多角形_484" data-name="多角形 484" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(144 248) rotate(90)" />
            <path id="多角形_485" data-name="多角形 485" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(72 248) rotate(90)" />
            <path id="多角形_495" data-name="多角形 495" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(1044 310) rotate(90)" />
            <path id="多角形_501" data-name="多角形 501" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(612 310) rotate(90)" />
            <path id="多角形_502" data-name="多角形 502" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(540 310) rotate(90)" />
            <path id="多角形_503" data-name="多角形 503" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(468 310) rotate(90)" />
            <path id="多角形_504" data-name="多角形 504" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(396 310) rotate(90)" />
            <path id="多角形_505" data-name="多角形 505" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(324 310) rotate(90)" />
            <path id="多角形_506" data-name="多角形 506" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(252 310) rotate(90)" />
            <path id="多角形_507" data-name="多角形 507" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(180 310) rotate(90)" />
            <path id="多角形_508" data-name="多角形 508" class="clss-4" d="M62.25,0,83,36,62.25,72H20.75L0,36,20.75,0Z" transform="translate(108 310) rotate(90)" />
        </g>
    </g>
</svg>