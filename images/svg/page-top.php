<svg xmlns="http://www.w3.org/2000/svg" width="16.707" height="9.061" viewBox="0 0 16.707 9.061">
    <defs>
        <style>
            .page-top-1 {
                fill: none;
                stroke: #3e366b;
                stroke-miterlimit: 10;
            }
        </style>
    </defs>
    <path id="パス_17" data-name="パス 17" class="page-top-1" d="M8.59,406.791l-8-8-8,8" transform="translate(7.764 -398.084)" />
</svg>