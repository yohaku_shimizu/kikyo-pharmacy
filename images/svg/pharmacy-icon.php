<svg id="icon_pharmacy" xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 48 48">
    <defs>
        <style>
            .cstm-fill {
                fill: #fff;
            }

            .pharmacy-2,
            .pharmacy-4 {
                fill: none;
            }

            .pharmacy-2 {
                stroke-miterlimit: 10;
                stroke-width: 4px;
            }

            .cstm-stroke {
                stroke: #fff;
            }

            .pharmacy-3 {
                stroke: none;
            }
        </style>
    </defs>
    <path id="パス_28762" data-name="パス 28762" class="cstm-fill" d="M0,0V19H48V0ZM42,10.786H38.786V14H36.214V10.786H33V8.214h3.214V5h2.572V8.214H42Z" />
    <g id="長方形_1965" data-name="長方形 1965" class="pharmacy-2 cstm-stroke" transform="translate(0 14)">
        <rect class="pharmacy-3" width="48" height="34" />
        <rect class="pharmacy-4" x="2" y="2" width="44" height="30" />
    </g>
    <g id="長方形_1966" data-name="長方形 1966" class="pharmacy-2 cstm-stroke" transform="translate(13 26)">
        <rect class="pharmacy-3" width="22" height="22" />
        <rect class="pharmacy-4" x="2" y="2" width="18" height="18" />
    </g>
    <rect id="長方形_1967" data-name="長方形 1967" class="cstm-fill" width="4" height="22" transform="translate(22 26)" />
</svg>