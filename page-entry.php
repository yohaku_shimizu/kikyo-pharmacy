<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/inco-under-entry') ?>
            <p>ENTRY</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<main id="entry">
    <section>
        <h2 class="green vertical-head">ENTRY</h2>
        <div class="page-width">
            <h3 class="title-border">エントリー</h3>
            <article class="double-card">
                <a href="<?= home_url() ?>/faq" class="double-link">
                    <div class="double-card--left">
                        <p class="double-card--left__title">FAQ</p>
                        <div class="double-card--left__content">
                            <p><?= get_template_part('/images/svg/top-card03'); ?></p>
                            <p class="card-text">よくある質問</p>
                        </div>
                    </div>
                </a>
                <a href="<?= home_url() ?>/recruit" class="double-link ">
                    <div class="double-card--right">
                        <p class="double-card--left__title">RECRUIT</p>
                        <div class="double-card--left__content">
                            <p><?= get_template_part('/images/svg/info-under-icon'); ?></p>
                            <p class="card-text">募集要項</p>
                        </div>
                    </div>
                </a>
            </article>
            <div class="entry-text">
                <p class="entry-text__item">「個人情報保護方針」をお読みになり、同意のうえご記入ください。</p>
                <p class="entry-text__item">ご返信までに2～3日かかることもございますので、お急ぎの方はお電話にてお問い合わせください。</p>
                <p class="entry-text__item">返信メールをお受け取りいただけるよう、受信設定（迷惑メール設定）等をお確かめください。</p>
                <p class="entry-text__item">万一、こちらから返信がない場合は、大変お手数ですが再度ご連絡ください。</p>
            </div>
        </div>
    </section>

    <section class="contact-form">
        <div class="page-width cf7">
            <?php echo do_shortcode('[contact-form-7 id="6" title="エントリー"]'); ?>
        </div>

    </section>

    <?php get_template_part('template-parts/intern') ?>

</main>

<?php get_footer(); ?>

<script>
    $(function() {
        var entry_pal = GetQueryString();

        if (entry_pal['type'] == '【中途採用】薬剤師') {
            $('input[name=radio-747]:eq(1)').prop('checked', true);
        } else if (entry_pal['type'] == '【新卒採用】薬剤師') {
            $('input[name=radio-747]:eq(0)').prop('checked', true);
        } else if (entry_pal['type'] == '【非常勤】薬剤師') {
            $('input[name=radio-747]:eq(2)').prop('checked', true);
        } else if (entry_pal['type'] == '【新卒採用】医療事務') {
            $('input[name=radio-747]:eq(3)').prop('checked', true);
        } else if (entry_pal['type'] == '【中途採用】医療事務') {
            $('input[name=radio-747]:eq(4)').prop('checked', true);
        } else if (entry_pal['type'] == '【非常勤】医療事務') {
            $('input[name=radio-747]:eq(5)').prop('checked', true);
        }


        if (entry_pal['pharmacy'] == 'ききょう薬局') {
            $('input[name=radio-401]:eq(0)').prop('checked', true);
        } else if (entry_pal['pharmacy'] == 'つつじ薬局') {
            $('input[name=radio-401]:eq(1)').prop('checked', true);
        } else if (entry_pal['pharmacy'] == 'すずらん薬局') {
            $('input[name=radio-401]:eq(2)').prop('checked', true);
        } else if (entry_pal['pharmacy'] == 'なのはな薬局') {
            $('input[name=radio-401]:eq(3)').prop('checked', true);
        } else if (entry_pal['pharmacy'] == 'さくら薬局') {
            $('input[name=radio-401]:eq(4)').prop('checked', true);
        } else if (entry_pal['pharmacy'] == 'あんしん薬局') {
            $('input[name=radio-401]:eq(5)').prop('checked', true);
        }
    });
</script>