<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/service-icon') ?>
            <p>SERVICE</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<main id="service">
    <section>
        <h2 class="vertical-head green">SERVICE</h2>
        <div class="page-width">
            <h3 class="title-border">調剤事業</h3>
            <article class="under-content">
                <div class="under-content--area">
                    <img src="<?= get_template_directory_uri() ?>/images/service/service1.jpg" alt="事業画像1">
                    <div class="under-content--text">
                        <div class="text-box box-r">
                            <p class="text-box__title">めざすのは「まちの薬屋さん」</p>
                            <p class="text-box__text">
                                保険調剤業務・在宅医療参画を中心に、市販薬・衛生用品・健康食品などの相談・販売を行う調剤薬局を運営しております。<br>
                                安心・安全で効率的な医療の向上を図るとともに、患者様お一人おひとりに寄り添い地域に密着した「まちの薬屋さん」として、患者様の健康をサポートしていきます。

                            </p>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section>
        <div class="page-width">
            <h3 class="title-border">在宅業務</h3>
            <article class="under-content">
                <div class="under-content--area">
                    <img src="<?= get_template_directory_uri() ?>/images/service/service2.jpg" alt="事業画像2">
                    <div class="under-content--text">
                        <div class="text-box box-r">
                            <p class="text-box__title">地域の皆様の健康を守る、在宅医療の強化</p>
                            <p class="text-box__text">
                                高齢化社会の進行などの背景からご自身で病院に通えず、十分な治療を受けられない患者様が増え、在宅医療が必要不可欠になっています。<br>
                                地域皆様の健康を守るため、医師・看護師・施設スタッフ・ケアマネジャーと連携をとり、高齢者施設や患者様宅を訪問し、お薬の説明やご相談を行っています。
                            </p>
                            <p class="title-s-border"><a href="<?= home_url() ?>/about/#partner">医療・介護で連携する皆様</a></p>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->

</main>

<?php get_footer(); ?>