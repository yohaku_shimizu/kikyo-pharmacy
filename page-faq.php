<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/faq-icon') ?>
            <p>FAQ</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>


<main id="faq">
    <section>
        <h2 class="green vertical-head">FAQ</h2>
        <div class="page-width">
            <div class="job-faq">
                <h3 class="title-border">求人に関するよくある質問</h3>
                <?php
                $career_flow = SCF::get('recruit_faq');
                $faq01_counter = 0;
                foreach ($career_flow as $value) : ?>
                    <div class="job-faq__content active button">
                        <?php if ($faq01_counter == 0) : ?>
                            <p class="job-faq__content--title active"><span class="q">Q</span><span><?php echo $value['recruit_faq_question']; ?></span><span class="btn"></span></p>
                            <p class="job-faq__content--text content_open"><?php echo nl2br($value['recruit_faq_answer']); ?></p>
                        <?php else : ?>
                            <p class="job-faq__content--title"><span class="q">Q</span><span><?php echo $value['recruit_faq_question']; ?></span><span class="btn"></span></p>
                            <p class="job-faq__content--text"><?php echo nl2br($value['recruit_faq_answer']); ?></p>
                        <?php endif; ?>
                    </div>
                    <?php $faq01_counter = 1; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section>
        <div class="page-width">
            <div class="job-faq">
                <h3 class="title-border">その他のよくある質問</h3>
                <?php
                $career_flow = SCF::get('other_faq');
                $faq02_counter = 0;
                foreach ($career_flow as $value) :
                ?>
                    <div class="job-faq__content active button">
                        <?php if ($faq02_counter == 0) : ?>
                            <p class="job-faq__content--title active"><span class="q">Q</span><span><?php echo $value['other_faq_question']; ?></span><span class="btn"></span></p>
                            <p class="job-faq__content--text content_open"><?php echo nl2br($value['other_faq_answer']); ?></p>
                        <?php else : ?>
                            <p class="job-faq__content--title"><span class="q">Q</span><span><?php echo $value['other_faq_question']; ?></span><span class="btn"></span></p>
                            <p class="job-faq__content--text"><?php echo nl2br($value['other_faq_answer']); ?></p>
                        <?php endif; ?>
                    </div>
                    <?php $faq02_counter = 1; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>
<script>
    $(function() {
        $('.job-faq__content--title').on('click', function() {
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });
    });
</script>
<?php get_footer(); ?>