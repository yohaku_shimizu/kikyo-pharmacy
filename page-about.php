<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>

<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/about-icon') ?>
            <p>ABOUT</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<main id="about">
    <section>
        <h2 class="vertical-head green">ABOUT</h2>
        <div class="page-width">
            <h3 class="title-border">会社概要</h3>
            <div class="about-content">
                <div class="about-content__flex">
                    <div class="about-content__flex--item">
                        <p class="title">名称</p>
                        <p class="text"><?= get_field('about_name'); ?></p>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">設立</p>
                        <p class="text"><?= get_field('about_established'); ?></p>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">資本金</p>
                        <p class="text"><?= get_field('about_capital'); ?></p>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">代表者</p>
                        <p class="text"><?= get_field('about_representative'); ?></p>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">所在地</p>
                        <div class="add-box">
                            <p class="text"><?= get_field('about_address'); ?></p>
                            <a class="map" href="https://goo.gl/maps/rCkdesy4zSQfM8uv5" target="_blank">
                                <img class="mapicon" src="<?= get_template_directory_uri() ?>/images/about/map_icon.svg" alt="マップアイコン" width="12" height="17">
                                Google Map
                                <img class="linkicon" src="<?= get_template_directory_uri() ?>/images/about/link.svg" alt="外部リンクアイコン" width="14" height="12">
                            </a>
                        </div>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">連絡先</p>
                        <div class="tel">
                            <p class="text">TEL</p>
                            <p class="text"><?= get_field('about_tel'); ?></p>
                        </div>
                        <div class="fax">
                            <p class="text">FAX</p>
                            <p class="text"><?= get_field('about_fax'); ?></p>
                        </div>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">E-mail</p>
                        <p class="text"><?= get_field('about_mail'); ?></p>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">従業員数</p>
                        <p class="text"><?= get_field('about_staff'); ?></p>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">事業内容</p>
                        <p class="text"><?= get_field('about_business'); ?></p>
                    </div>
                    <div class="about-content__flex--item">
                        <p class="title">店舗展開</p>
                        <p class="text"><?= get_field('about_store'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="partner">
        <div class="page-width">
            <h3 class="title-border">パートナー企業</h3>
            <div class="partner-slick">
                <?php
                $groupA = SCF::get('partner');
                foreach ($groupA as $fields) :
                    $imgurl = wp_get_attachment_image_src($fields['partner_img'], 'full');
                ?>
                    <div>
                        <img src="<?= $imgurl[0] ?>" alt="">
                        <p><?= $fields['partner_name']; ?></p>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <?php get_template_part('template-parts/intern'); ?>
</main>

<script>
    //パートナー企業のslick設定
    $('.partner-slick').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: true,
        arrows: false,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1500,
        responsive: [{
            breakpoint: 540,
            settings: {
                slidesToScroll: 2,
                slidesToShow: 2,
            }
        }]
    });
</script>
<?php get_footer(); ?>