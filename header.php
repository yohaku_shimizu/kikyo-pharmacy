<?php
// Internet Explorer で開かれた場合はedgeへ開くように通知を出す
$edge_open = true;

if (isset($_COOKIE['view_ie'])) {
  if ($edge_open && $_COOKIE['view_ie'] != 'on') {
    if (get_browser_name() == "ie") { ?>
      <script>
        MoveCheck();

        function MoveCheck() {
          if (confirm("ご利用のウェブページはInternet Explorerでの表示を推奨していません。Microsoft Edgeで表示しますか？")) {
            var url = location.href;
            url = "microsoft-edge:" + url;
            window.location.href = url;
          } else {
            alert("Internet Explorerでの表示を続行します。");
          }
        }
      </script>
<?php
      // ページ推移先で通知が出続けないようにクッキーにInternet Explorerで閲覧したフラグを残す
      // クッキーの有効時間 : 1時間
      setcookie('view_ie', 'on', time() + 60 * 60);
    }
  }
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="description" content="<?php seo_description(); ?>">
  <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
  <?php wp_head(); ?>

  <!-- lottie cdn -->
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.6.6/lottie.min.js" type="text/javascript"></script> -->


  <script>
    (function(d) {
      var config = {
          kitId: 'lnu0tgu',
          scriptTimeout: 3000,
          async: true
        },
        h = d.documentElement,
        t = setTimeout(function() {
          h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
        }, config.scriptTimeout),
        tk = d.createElement("script"),
        f = false,
        s = d.getElementsByTagName("script")[0],
        a;
      h.className += " wf-loading";
      tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
      tk.async = true;
      tk.onload = tk.onreadystatechange = function() {
        a = this.readyState;
        if (f || a && a != "complete" && a != "loaded") return;
        f = true;
        clearTimeout(t);
        try {
          Typekit.load(config)
        } catch (e) {}
      };
      s.parentNode.insertBefore(tk, s)
    })(document);
  </script>

  <script>
    (function(d) {
      var config = {
          kitId: 'lnu0tgu',
          scriptTimeout: 3000,
          async: true
        },
        h = d.documentElement,
        t = setTimeout(function() {
          h.className = h.className.replace(/\bwf-loading\b/g, "") + " wf-inactive";
        }, config.scriptTimeout),
        tk = d.createElement("script"),
        f = false,
        s = d.getElementsByTagName("script")[0],
        a;
      h.className += " wf-loading";
      tk.src = 'https://use.typekit.net/' + config.kitId + '.js';
      tk.async = true;
      tk.onload = tk.onreadystatechange = function() {
        a = this.readyState;
        if (f || a && a != "complete" && a != "loaded") return;
        f = true;
        clearTimeout(t);
        try {
          Typekit.load(config)
        } catch (e) {}
      };
      s.parentNode.insertBefore(tk, s)
    })(document);
  </script>

  <link rel="icon" href="<?= get_template_directory_uri() ?>/images/front/favicon.svg" type="image/svg+xml">
  <!-- ファビコン設定 -->
  <link rel="shortcut icon" href="<?= get_template_directory_uri() ?>/images/front/favicon.ico">
  <link rel="apple-touch-icon" href="<?= get_template_directory_uri() ?>/images/front/apple-touch-icon.png" sizes="180x180">
  <link rel="icon" type="image/png" href="<?= get_template_directory_uri() ?>/images/front/android-touch-icon.png" sizes="192x192">


</head>

<!-- get template directory uri for Javascript -->

<body class="body_<?php echo get_page_slug() ?>">
  <div class="wrap">

    <?php

    if (WP_DEBUG && current_user_can('manage_options')) {
      include_once('debug/variables.php');
      global $debug_var;
      $debug_var = new DebugVariables();
    }
    ?>

    <header>
      <div class="header-flex">
        <h1 class="pc"><a href="<?= home_url() ?>"><img src="<?= get_template_directory_uri() ?>/images/front/logo.svg" alt="ロゴ"></a></h1>
        <h1 class="sp"><a href="<?= home_url() ?>"><img src="<?= get_template_directory_uri() ?>/images/front/logo-sp.svg" alt="ロゴ"></a></h1>
        <!-- <div class="visit"><a href="##">VISIT</a></div> -->
        <div class="entry"><a href="<?= home_url() ?>/entry">ENTRY</a></div>
        <div class="hamburger button">
          <span></span>
          <span></span>
        </div>
      </div>
      <nav class="globalMenuSp">
        <div class="menu-items">
          <div class="menu-items__logo"><a href="<?= home_url() ?>"><img src="<?= get_template_directory_uri() ?>/images/front/logo-w.svg" alt="ロゴ"></a></div>
          <div class="menu-items__corporate">
            <p class="menu-items__corporate--title ac-parent button">CORPORATE</p>
            <div class="items-box ac-child item-active">
              <p class="items-box__item"><a href="<?= home_url() ?>/about">キキョウファーマシーグループについて</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/service">事業内容</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/pharmacy">店舗一覧</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/news">お知らせ</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/contact">お問い合わせ</a></p>
            </div>
          </div>
          <div class="menu-items__corporate ">
            <p class="menu-items__corporate--title ac-parent button">RECRUIT</p>
            <div class="items-box ac-child">
              <p class="items-box__item"><a href="<?= home_url() ?>/voice">代表メッセージと社員インタビュー</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/career">教育・キャリアプラン</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/info">数字で見る</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/oneday">1日の流れ</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/ict">ICTの活用</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/recruit">募集要項</a></p>
              <p class="items-box__item"><a href="<?= home_url() ?>/faq">よくある質問</a></p>
            </div>
          </div>
          <div class="menu-items__buttons">
            <!-- <a class="menu-items__buttons--btn" href="##">
                <p>職場見学予約<?php get_template_part('/images/svg/link-icon'); ?></p>
              </a> -->
            <a class="menu-items__buttons--btn" href="<?= home_url() ?>/entry">
              <p>エントリー</p>
            </a>
          </div>
        </div>
      </nav>
    </header>


    <div id="cursor" class="cursor"></div>