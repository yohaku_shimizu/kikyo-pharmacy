$(function() {
    $.fn.hexa_anime = function(options) {

        // オプションデフォルト値
        var defaults = {
            delay: 4000, //遅延読み込み 
            reverse: false, //pathの色を反転するか 
            fill: true, //透明にアニメーションするか　
            speed: false
        };

        // オプション定義された内容を入れる
        if (typeof options.speed != 'undefined') {
            defaults.speed = options.speed;
        }

        // 出現アニメーション
        $(this).addClass('done');
        var i = 1;
        $(this).find("path").each(function() {
            $(this).addClass('animation' + i++);
        });

        // ループアニメーション
        if (!defaults.speed) {
            if (defaults.fill == true) {
                setTimeout($.proxy(function() {
                    $(this).find("path").each(function() {
                        $(this).addClass('rmd-animation' + Math.round(Math.random() * 65));
                    });
                }, this), defaults.delay);
            };
        } else {
            if (defaults.fill == true) {
                setTimeout($.proxy(function() {
                    $(this).find("path").each(function() {
                        $(this).addClass('rmd-animation-high' + Math.round(Math.random() * 65));
                    });
                }, this), defaults.delay);
            };
        }
    }
});