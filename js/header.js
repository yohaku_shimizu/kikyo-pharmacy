$(function() {
    $('.hamburger').click(function() {
        $(this).toggleClass('active');

        if ($(this).hasClass('active')) {
            $('.globalMenuSp').addClass('active');
        } else {
            $('.globalMenuSp').removeClass('active');
        }
    });
});


$(function() {
    $('.ac-parent').on('click', function() {
        $(this).next().slideToggle();
        //openクラスをつける
        $(this).toggleClass("open");
        //クリックされていないac-parentのopenクラスを取る
        // $('.ac-parent').not(this).removeClass('open');

        // // 一つ開くと他は閉じるように
        // $('.ac-parent').not($(this)).next('.ac-child').slideUp();
    });
});


// カーソル用のdivタグを取得してcursorに格納
var cursor = document.getElementById('cursor');

// カーソル用のdivタグをマウスに追従させる
document.addEventListener('mousemove', function(e) {
    cursor.style.transform = 'translate(' + e.clientX + 'px, ' + e.clientY + 'px)';
});

// リンクにホバーした時にクラス追加、離れたらクラス削除
var link = document.querySelectorAll('a');
for (var i = 0; i < link.length; i++) {
    link[i].addEventListener('mouseover', function(e) {
        cursor.classList.add('cursor--hover');
    });
    link[i].addEventListener('mouseout', function(e) {
        cursor.classList.remove('cursor--hover');
    });
}

$('.button').hover(
    function() {
        //マウスカーソルが重なった時の処理
        cursor.classList.add('cursor--hover');
    },
    function() {
        //マウスカーソルが離れた時の処理
        cursor.classList.remove('cursor--hover');
    }
);