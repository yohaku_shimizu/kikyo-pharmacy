<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/recruit_under') ?>
            <p>RECRUIT</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<?php $theme_options = get_option('theme_option_name'); ?>


<main id="recruit">
    <section>
        <h2 class="green vertical-head">RECRUIT</h2>
        <div class="page-width">
            <h3 class="title-border">募集店舗一覧</h3>
            <div class="pharmacy-flex">
                <div class="pharmacy-left">
                    <?php if (have_posts()) : ?>
                        <?php $pharmacy = []; ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <?php $reccomendItem = get_field('pharmacy'); ?>
                            <?php if ($reccomendItem) : ?>
                                <?php foreach ($reccomendItem as $key => $value) : ?>
                                    <?php
                                    $taxonomy_slug = 'recruit_category';
                                    $category_terms = wp_get_object_terms($post->ID, $taxonomy_slug);
                                    if (!empty($category_terms)) { ?>
                                        <div class="pharmacy-title-area">
                                            <p class=" title"><?php echo $value->post_title ?></p>
                                            <?php
                                            if (!is_wp_error($category_terms)) {
                                                echo '<ul>';
                                                foreach ($category_terms as $category_term) {
                                                    echo '<li><a href="' . get_term_link($category_term->slug, $taxonomy_slug) . '">' . $category_term->name . '</a></li>'; // タームをリンク付きで表示
                                                }
                                                echo '</ul>';
                                            }
                                            ?>
                                        </div>
                                        <div class="pharmacy-area">
                                            <img src="<?= get_field('pharmacy_image', $value->ID) ?>" alt="薬局画像">
                                            <div class="pharmacy-area__right">
                                                <p><?= get_field('pharmacy_addresnum', $value->ID) ?></p>
                                                <p><?= get_field('pharmacy_addres', $value->ID) ?></p>
                                                <p>TEL <?= get_field('pharmacy_phone2', $value->ID) ?></p>
                                                <p>夜間等緊急連絡先 <?= get_field('pharmacy_phone', $value->ID) ?></p>
                                                <p>FAX <?= get_field('pharmacy_fax', $value->ID) ?></p>
                                                <p>営業時間 <?= nl2br(get_field('pharmacy_time', $value->ID)) ?></p>
                                                <p>定休日 <?= get_field('pharmacy_holiday', $value->ID) ?></p>
                                                <p class="title-s-border"><a href="<?= get_the_permalink($value->ID); ?>">求人情報を見る</a></p>
                                                <?php $pharmacy[$value->post_title] = $value; ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>


                <!-- カテゴリー一覧 ======================================================================== -->
                <div class="side-nav">
                    <div class="side-nav-category">
                        <div class="ac-parent">
                            <h3 class="title-border button">CATEGORY</h3>
                        </div>
                        <div class="category ac-child">
                            <ul><?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'recruit_category', 'show_count' => 1)); ?></ul>

                        </div>
                    </div>
                    <!-- カテゴリー一覧 ======================================================================== -->

                    <!-- 年一覧 ================================================================================ -->
                    <div class="side-nav-year">

                        <div class="year">
                            <div class="ac-parent">
                                <h3 class="title-border button">AREA</h3>
                            </div>
                            <ul class="ac-child"><?php wp_list_categories(array('title_li' => '', 'taxonomy' => 'recruit_area', 'show_count' => 1)); ?></ul>

                            <!-- 年一覧 ================================================================================ -->
                        </div>
                    </div>
                </div>

            </div>
    </section>


    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>

<?php get_footer(); ?>