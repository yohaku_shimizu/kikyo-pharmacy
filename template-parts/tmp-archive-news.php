<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>

<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/new-icon') ?>
            <p>NEWS</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<section id="news">
    <h2 class="green vertical-head">NEWS</h2>
    <div class="page-width side-content">
        <div class="news-area">
            <h3 class="title-border">ニュース</h3>
            <div class="side-left">
                <?php
                if (have_posts()) :
                ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <article class="up_news">

                            <div class="article__option">
                                <p class="article__option--time"><?php echo get_the_date(); ?></p>
                                <p class="article__option--category">
                                    <?php
                                    $category = get_the_category();
                                    if ($category[0]) {
                                        echo '<a href="' . get_category_link($category[0]->term_id) . '">' . $category[0]->cat_name . '</a>';
                                    }
                                    ?>
                                </p>
                            </div>
                            <p class="article__title"><a href="<?php echo get_permalink(); ?>" class="article"><?php the_title(); ?></a></p>

                        </article>
                    <?php endwhile; ?>
                <?php endif; ?>

                <?php
                /* ページャーの表示 
       クエリの変数を間違えないように
    */
                global $wp_query;
                if (function_exists('pagination')) :
                    pagination($wp_query->max_num_pages, (get_query_var('paged')) ? get_query_var('paged') : 1);  //$wp_query ではなく $the_query ないことに注意！
                endif;
                ?>
            </div>
        </div>

        <!-- カテゴリー一覧 ======================================================================== -->
        <div class="side-nav">
            <div class="side-nav-category">
                <div class="ac-parent">
                    <h3 class="title-border button">CATEGORY</h3>
                </div>

                <div class="category ac-child">
                    <ul>
                        <?php
                        $categories = get_categories();
                        $separator = "";
                        $output = "";
                        if ($categories) {
                            foreach ($categories as $category) {
                                $output .= '<li><a href="' . get_category_link($category->term_id) . '">' . $category->cat_name . ' (' . $category->count . ')' . '</a>' . $separator . '</li>';
                            }
                            echo trim($output, $separator);
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <!-- カテゴリー一覧 ======================================================================== -->

            <!-- 年一覧 ================================================================================ -->
            <div class="side-nav-year">

                <div class="year">
                    <div class="ac-parent">
                        <h3 class="title-border button">YEAR</h3>
                    </div>

                    <ul class="ac-child">
                        <?php wp_get_archives('post_type=post&type=yearly&show_post_count=1'); ?>
                    </ul>
                </div>
                <!-- 年一覧 ================================================================================ -->
            </div>
        </div>

    </div>
</section>

<!-- インターンシップ呼び出し -->
<?php get_template_part('template-parts/intern'); ?>
<!-- インターンシップ呼び出し -->
<?php get_footer(); ?>