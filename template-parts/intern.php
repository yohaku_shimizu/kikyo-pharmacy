<?php $theme_options = get_option('theme_option_name'); ?>
<?php if (!empty($theme_options['op_6'])) : ?>
    <section id="intern">
        <div class="intern-width page-width">
            <article class="intern-area box">
                <h3 class="intern-area__title">インターンシップ</h3>
                <p class="intern-area__text"><?php echo $theme_options['op_5']; ?></p>
                <div class="intern-info">
                    <p class="intern-info__tel">TEL <a href="tel:<?php echo $theme_options['op_1']; ?>"><?php echo $theme_options['op_1']; ?></a></p>
                    <p class="intern-info__person">担当： <?php echo $theme_options['op_2']; ?></p>
                    <p class="intern-info__time">受付時間 <?php echo $theme_options['op_3']; ?></p>
                    <p class="intern-info__holiday">定休日 <?php echo $theme_options['op_4']; ?></p>
                </div>
            </article>
        </div>

        <?php echo get_template_part('/images/svg/bg_hexagon2'); ?>

    </section>
<?php else : ?>
<?php endif; ?>