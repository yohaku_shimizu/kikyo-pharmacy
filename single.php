<?php get_header(); ?>
<div class="main-box">
  <div class="page-width main-wrap">
    <div class="main-title">
      <?php get_template_part('images/svg/new-icon') ?>
      <p>NEWS</p>
    </div>
  </div>
</div>
<div class="page-width">
  <?php get_template_part('template-parts/breadcrumb'); ?>
</div>



<main id="single">
  <section id="news" class="section single-news">
    <h2 class="green vertical-head">NEWS</h2>
    <div class="page-width side-content">
      <div class="news-area">
        <article class="section_inner single-content">

          <div class="single-content__time date">
            <?php the_time('Y/m/d') ?>
            <span class="single-category"><?php the_category(',') ?></span>
          </div>
          <div class="single-content__title">
            <p class="text-24"><?php the_title(); ?></p>
          </div>

          <div class="single-content__img">
            <?php
            $news_img_url = get_the_post_thumbnail_url(get_the_ID(), 'medium');
            if ($news_img_url) :
            ?>
              <img src="<?php the_post_thumbnail_url() ?>" alt="">
            <?php else : ?>
              <img src="<?php echo get_template_directory_uri(); ?>/images/single/default.png" alt="">
            <?php endif; ?>
          </div>

          <div class="single-content__text">
            <?= the_content(); ?>
          </div>

          <div class="single-content__page">
            <?php previous_post_link('%link', '<img src="' . get_template_directory_uri() . '/images/news/prev-arrow.svg" alt="前のニュースへ">'); ?>
            <a href="<?= home_url("/news") ?>" class="tolist">お知らせ一覧</a>
            <?php next_post_link('%link', '<img src="' . get_template_directory_uri() . '/images/news/next-arrow.svg" alt="前のニュースへ">'); ?>
          </div>
        </article>
      </div>

      <!-- カテゴリー一覧 ======================================================================== -->
      <div class="side-nav">
        <div class="side-nav-category">
          <div class="ac-parent">
            <h3 class="title-border button">CATEGORY</h3>
          </div>

          <div class="category ac-child">
            <ul>
              <?php
              $categories = get_categories();
              $separator = "";
              $output = "";
              if ($categories) {
                foreach ($categories as $category) {
                  $output .= '<li><a href="' . get_category_link($category->term_id) . '">' . $category->cat_name . ' (' . $category->count . ')' . '</a>' . $separator . '</li>';
                }
                echo trim($output, $separator);
              }
              ?>
            </ul>
          </div>
        </div>
        <!-- カテゴリー一覧 ======================================================================== -->

        <!-- 年一覧 ================================================================================ -->
        <div class="side-nav-year">

          <div class="year">
            <div class="ac-parent">
              <h3 class="title-border button">YEAR</h3>
            </div>

            <ul class="ac-child">
              <?php wp_get_archives('post_type=post&type=yearly&show_post_count=1'); ?>
            </ul>
          </div>
          <!-- 年一覧 ================================================================================ -->
        </div>
      </div>
    </div>
  </section>

  <!-- インターンシップ呼び出し -->
  <?php get_template_part('template-parts/intern'); ?>
  <!-- インターンシップ呼び出し -->


</main>

<?php get_footer(); ?>