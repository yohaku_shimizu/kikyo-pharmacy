<?php get_header(); ?>

<?php get_template_part('template-parts/loading'); ?>

<div class="hexa-wrap">
    <div class="main-visual-box">
        <div class="main-visual" style="background-image: url(<?php echo get_theme_mod('main-visual'); ?>);"></div>
    </div>
    <p class="main-text"><?php echo nl2br(get_theme_mod('main-text')); ?></p>
    <?php get_template_part('/images/svg/mv-hexagon');
    ?>
    <ul class="main-news">
        <?php
        $args = array(
            'posts_per_page' => 1 // 表示件数の指定
        );
        $posts = get_posts($args);
        foreach ($posts as $post) : // ループの開始
            setup_postdata($post); // 記事データの取得
        ?>
            <li>
                <span class="tag">NEWS</span>
                <a href="<?php the_permalink(); ?>">
                    <div class="news-box">
                        <span class="main-news-date"><?php echo get_the_date(); ?></span>
                        <?php
                        // 記事のカテゴリー情報を取得する
                        $cat = get_the_category();
                        // 取得した配列から必要な情報を変数に入れる
                        $cat_name = $cat[0]->cat_name; // カテゴリー名
                        ?>
                        <span class="main-news-cat"><?= $cat_name; ?></span>
                    </div>
                    <span class="main-news-title"><?php the_title(); ?></span>
                </a>
            </li>
        <?php
        endforeach;
        wp_reset_postdata();
        ?>
    </ul>
</div>


<main class="front">
    <section id="front-01">
        <h2 class="white vertical-head">CORPORATE</h2>
        <div class="page-width corporate">
            <article class="front-button">
                <a class="ftont-link" href="<?= home_url() ?>/about">
                    <div class="front-content">
                        <div class="front-image">
                            <div class="front-title box-l">
                                <p class="front-title-icon"><?= get_template_part('/images/svg/about-icon'); ?></p>
                                <p class="front-title-text">ABOUT</p>
                            </div>
                            <div class="box img-wrap">
                                <div class="img-left" style="background-image: url(<?php echo get_theme_mod('front-content-about'); ?>);"></div>
                            </div>
                            <p class="front-text box-r">
                                「まちのかかりつけ薬局」として<br>
                                医療への貢献を目指します
                            </p>
                        </div>
                    </div>
                </a>
            </article>

            <article class="front-button-r front-pt">
                <a href="<?= home_url() ?>/service">
                    <div class="front-content">
                        <div class="front-image">
                            <div class="front-title box-r">
                                <p class="front-title-icon"><?= get_template_part('/images/svg/service-icon'); ?></p>
                                <p class="front-title-text">SERVICE</p>
                            </div>
                            <div class="img-wrap box">
                                <div class="img-right" style="background-image: url(<?php echo get_theme_mod('front-content-service'); ?>);"></div>
                            </div>
                            <p class="front-text box-l">
                                医療・介護の皆様と連携して<br>
                                調剤事業・在宅業務を行っています
                            </p>
                        </div>
                    </div>
                </a>
            </article>


            <article class="front-button front-pt">
                <a href="<?= home_url() ?>/pharmacy">
                    <div class="front-content">
                        <div class="front-image">
                            <div class="front-title box-l">
                                <p class="front-title-icon"><?= get_template_part('/images/svg/pharmacy-icon'); ?></p>
                                <p class="front-title-text">PHARMACY</p>
                            </div>
                            <div class="img-wrap box">
                                <div class="img-left" style="background-image: url(<?php echo get_theme_mod('front-content-pharmacy'); ?>);"></div>
                            </div>
                            <p class="front-text box-r">
                                キキョウファーマシーグループが<br>
                                運営する店舗一覧
                            </p>
                        </div>
                    </div>
                </a>
            </article>

        </div>
        <?php echo get_template_part('/images/svg/bg_hexagon'); ?>

    </section>


    <section id="front-02">
        <h2 class="gray vertical-head">RECRUIT</h2>
        <div class="page-width recruit">
            <article class="front-button-r ">
                <a href="<?= home_url() ?>/voice">
                    <div class="front-content">
                        <div class="front-image">
                            <div class="front-title box-r">
                                <p class="front-title-icon"><?= get_template_part('/images/svg/voice-icon'); ?></p>
                                <p class="front-title-text">VOICE</p>
                            </div>
                            <div class="img-wrap box">
                                <div class="img-right" style="background-image: url(<?php echo get_theme_mod('front-content-voice'); ?>);"></div>
                            </div>
                            <p class="front-text box-l">
                                キキョウファーマシーグループ<br>
                                代表メッセージと社員インタビュー
                            </p>
                        </div>
                    </div>
                </a>
            </article>

            <article class="front-button front-pt">
                <a href="<?= home_url() ?>/career">
                    <div class="front-content">
                        <div class="front-image">
                            <div class="front-title box-l">
                                <p class="front-title-icon"><?= get_template_part('/images/svg/career-icon'); ?></p>
                                <p class="front-title-text">CAREER PLAN</p>
                            </div>
                            <div class="img-wrap box">
                                <div class="img-left" style="background-image: url(<?php echo get_theme_mod('front-content-career'); ?>);"></div>
                            </div>
                            <p class="front-text box-r">
                                キャリアを構成する<br>
                                研修制度
                            </p>
                        </div>
                    </div>
                </a>
            </article>
        </div>

        <div class="page-width">
            <article class="double-card ">
                <a href="<?= home_url() ?>/info" class="double-link">
                    <div class="double-card--left">
                        <p class="double-card--left__title box-t">INFOGRAPHICS</p>
                        <div class="double-card--left__content box-b">
                            <p><?= get_template_part('/images/svg/top-card01'); ?></p>
                            <p class="card-text">数字で見る</p>
                        </div>
                    </div>
                </a>
                <a href="<?= home_url() ?>/oneday" class="double-link">
                    <div class="double-card--right ">
                        <p class="double-card--left__title box-t">ONE DAY</p>
                        <div class="double-card--left__content box-b">
                            <p><?= get_template_part('/images/svg/top-card02'); ?></p>
                            <p class="card-text">一日の流れ</p>
                        </div>
                    </div>
                </a>
            </article>
        </div>
        <div class="page-width recruit">
            <article class="front-button-r front-pt2">
                <a href="<?= home_url() ?>/ict">
                    <div class="front-content">
                        <div class="front-image">
                            <div class="front-title box-r">
                                <p class="front-title-icon"><?= get_template_part('/images/svg/ict-icon'); ?></p>
                                <p class="front-title-text">ICT</p>
                            </div>
                            <div class="img-wrap box">
                                <div class="img-right" style="background-image: url(<?php echo get_theme_mod('front-content-ict'); ?>);"></div>
                            </div>
                            <p class="front-text box-l">
                                最新機器導入による<br>
                                高い安全性の確立
                            </p>
                        </div>
                    </div>
                </a>
            </article>
            <article class="front-button front-pt">
                <a href="<?= home_url() ?>/recruit">
                    <div class="front-content">
                        <div class="front-image">
                            <div class="front-title box-l">
                                <p class="front-title-icon"><?= get_template_part('/images/svg/recruit-icon'); ?></p>
                                <p class="front-title-text">RECRUIT</p>
                            </div>
                            <div class="img-wrap box">
                                <div class="img-left" style="background-image: url(<?php echo get_theme_mod('front-content-recruit'); ?>);"></div>
                            </div>
                            <p class="front-text box-r">
                                募集要項はこちら
                            </p>
                        </div>
                    </div>
                </a>
            </article>
        </div>
        <div class="page-width">
            <article class="double-card">
                <a href="<?= home_url() ?>/faq" class="double-link box">
                    <div class="double-card--left">
                        <p class="double-card--left__title box-t">FAQ</p>
                        <div class="double-card--left__content box-b">
                            <p><?= get_template_part('/images/svg/top-card03'); ?></p>
                            <p class="card-text">よくある質問</p>
                        </div>
                    </div>
                </a>
                <a href="<?= home_url() ?>/entry" class="double-link box">
                    <div class="double-card--right">
                        <p class="double-card--left__title box-t">ENTRY</p>
                        <div class="double-card--left__content box-b">
                            <p><?= get_template_part('/images/svg/top-card04'); ?></p>
                            <p class="card-text">エントリー</p>
                        </div>
                    </div>
                </a>
            </article>
        </div>

    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->

</main>

<?php get_footer(); ?>