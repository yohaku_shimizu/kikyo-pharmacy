<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/voice-icon') ?>
            <p>VOICE</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<main id="voice">
    <section>
        <h2 class="vertical-head green">VOICE</h2>
        <div class="page-width">
            <h3 class="title-border">代表メッセージ</h3>
            <article class="under-content">
                <div class="under-content--area">
                    <img src="<?= get_field('representative_top_image') ?>" alt="代表の写真">
                    <div class="under-content--text">
                        <div class="text-box box-r">
                            <p class="text-box__title"><?= get_field('representative_title') ?></p>
                            <p class="text-box__text">
                                <?= nl2br(get_field('representative_message')) ?>
                            </p>
                            <p class="text-box__c-name">キキョウファーマシーグループ</p>
                            <p class="text-box__name"><span>代表取締役</span><?= get_field('representative_name') ?></p>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>

    <section>
        <div class="page-width">
            <h3 class="title-border">代表インタビュー</h3>
            <img src="<?= get_field('representative_interview_image') ?>" alt="代表の写真2">

            <article class="under-content">
                <div class="under-content--area">
                    <div class="under-content--text">
                        <?php
                        $interview = SCF::get('representative_interview');
                        $cnt = 0;
                        foreach ($interview as $value) : ?>
                            <div class="daihyou box">
                                <?php if ($cnt == 0) : ?>
                                    <a class="text-box__title staff_interview-area--title active">
                                        <p class="title-flex"><span class="q">Q</span><?php echo $value['representative_question']; ?><span class="btn"></span></p>
                                    </a>

                                    <div class="text-box active_content">

                                    <?php else : ?>
                                        <a class="text-box__title staff_interview-area--title">
                                            <p class="title-flex"><span class="q">Q</span><?php echo $value['representative_question']; ?><span class="btn"></span></p>
                                        </a>
                                        <div class="text-box">

                                        <?php endif; ?>
                                        <p class="text-box__text">
                                            <?= nl2br($value['representative_answer']) ?>
                                        </p>
                                        </div>
                                    </div>
                                    <?php $cnt = 1; ?>
                                <?php endforeach; ?>
                            </div>
                    </div>
            </article>
        </div>
    </section>

    <section>
        <div class="page-width">
            <h3 class="title-border">社員インタビュー</h3>
            <?php
            $interview = SCF::get('staff_interview');
            $counter_staff = 0;
            foreach ($interview as $value) :
                $imgurl = wp_get_attachment_image_src($value['staff_interview_image'], 'full');
            ?>
                <div class="staff_interview-area box">

                    <img src="<?php echo $imgurl[0] ?>" alt="社員の写真">
                    <div class="staff_interview-area--box">
                        <?php if ($counter_staff == 0) : ?>
                            <a class="staff_interview-area--title active button">
                                <p><span><?php echo $value['staff_interview_title']; ?></span><span class="btn"></span></p>
                            </a>

                            <div class="staff_interview-area--content active_content">

                            <?php else : ?>
                                <a class="staff_interview-area--title button">
                                    <p><span><?php echo $value['staff_interview_title']; ?></span><span class="btn"></span></p>
                                </a>
                                <div class="staff_interview-area--content">

                                <?php endif; ?>
                                <article class="under-content">
                                    <div class="under-content--area">
                                        <p class="text-box__text"><?php echo nl2br($value['staff_interview_content']); ?></p>
                                        <div class="text-box__name">
                                            <p class="text-box__name--college"><?php echo $value['staff_interview_college']; ?></p>
                                            <p class="text-box__name--name"><?php echo $value['staff_interview_name']; ?></p>
                                        </div>
                                    </div>
                                </article>
                                </div>
                            </div>
                            <?php $counter_staff++; ?>
                    </div>
                <?php endforeach; ?>
                </div>
    </section>

    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->

</main>

<?php get_footer(); ?>
<script>
    $(function() {
        $('.staff_interview-area--title').on('click', function() {
            $(this).toggleClass('active');
            $(this).next().slideToggle();
        });
    });
</script>