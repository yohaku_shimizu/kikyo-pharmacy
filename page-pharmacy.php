<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/pharmacy-under') ?>
            <p>PHARMACY</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>


<main id="pharmacy">
    <section id="pharmacy-sec">
        <h2 class="green vertical-head">PHARMACY</h2>
        <div class="page-width">

            <?php // タクソノミー(カテゴリ)別に記事を一覧出力
            $terms = get_terms('pharmacy_area');
            $pharmacy_count = 1;

            foreach ($terms as $term) :
                $args = array(
                    'post_type' => 'pharmacy_list',
                    'taxonomy' => 'pharmacy_area',
                    'order' => 'ASC',
                    'term' => $term->slug,
                    'posts_per_page' => -1,
                    'no_found_rows' => true,
                );
                $query = new WP_Query($args); ?>
                <article class="pharmacy">
                    <h3 class="title-border"><?php echo esc_html($term->name); ?></h3>
                    <?php if ($query->have_posts()) : ?>
                        <?php while ($query->have_posts()) : $query->the_post(); ?>
                            <div class="pharmacy-area" id="pharmacy_count_<?= $pharmacy_count; ?>">
                                <img src="<?php echo get_field('pharmacy_image') ?>" class="box-l" alt="薬局画像">
                                <div class="pharmacy-area__right box-r">
                                    <p class="pharmacy-title"><?= get_the_title(); ?></p>
                                    <p><?= get_field('pharmacy_addresnum') ?></p>
                                    <p><?= get_field('pharmacy_addres') ?></p>
                                    <p>夜間等緊急連絡先 <?= get_field('pharmacy_phone') ?></p>
                                    <p>FAX <?= get_field('pharmacy_fax') ?></p>
                                    <p>営業時間 <?= nl2br(get_field('pharmacy_time')) ?></p>
                                    <p>定休日 <?= get_field('pharmacy_holiday') ?></p>
                                    <div class="plus">
                                        <p class="title-s-border"><a href="tel:<?= get_field('pharmacy_phone2') ?>">TEL <?= get_field('pharmacy_phone2') ?></a></p>
                                        <p class="title-s-border"><a class="google-map" target="_blank" href="https://www.google.co.jp/maps/place/<?= get_field('pharmacy_addres') ?>"><?php get_template_part('/images/svg/google-map'); ?>Google Map<?php get_template_part('/images/svg/link-k-icon'); ?></a></p>
                                        <p class="title-s-border"><a href="<?= get_the_permalink(); ?>">求人情報を見る</a></p>
                                    </div>
                                </div>
                            </div>
                            <?php $pharmacy_count++; ?>
                        <?php endwhile; ?>
                </article>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
            <?php $pharmacy_count++; ?>

        <?php endforeach; ?>
        <!-- 準備中の求人です、準備中がない場合はコメントアウトしてください。 -->
        <div class="pharmacy-area preparation">
            <img src="<?= get_template_directory_uri() ?>/images/preparation.png" class="box-l" alt="準備中">
            <div class="pharmacy-area__right box-r">
                <p class="preparationtitle">準備中</p>
            </div>
        </div>
        <!------------------- ここまで ------------------->
        </div>
    </section>


    <!-- インターンシップ呼び出し -->
    <?php get_template_part('template-parts/intern'); ?>
    <!-- インターンシップ呼び出し -->
</main>

<?php get_footer(); ?>