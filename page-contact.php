<?php get_header(); ?>
<?php get_template_part('template-parts/under_loading'); ?>
<div class="main-box">
    <div class="page-width main-wrap">
        <div class="main-title">
            <?php get_template_part('images/svg/contact-icon') ?>
            <p>CONTACT</p>
        </div>
    </div>
</div>
<div class="page-width">
    <?php get_template_part('template-parts/breadcrumb'); ?>
</div>

<main id="contact">
    <section>

        <h2 class="green vertical-head">CONTACT</h2>
        <div class="page-width">
            <h3 class="title-border">お問い合わせ</h3>
            <div class="entry-text">
                <p class="entry-text__item">「個人情報保護方針」をお読みになり、同意のうえご記入ください。</p>
                <p class="entry-text__item">ご返信までに2～3日かかることもございますので、お急ぎの方はお電話にてお問い合わせください。</p>
                <p class="entry-text__item">返信メールをお受け取りいただけるよう、受信設定（迷惑メール設定）等をお確かめください。</p>
                <p class="entry-text__item">万一、こちらから返信がない場合は、大変お手数ですが再度ご連絡ください。</p>
            </div>
        </div>

    </section>

    <section class="contact-form">
        <div class="page-width">
            <?php echo do_shortcode('[contact-form-7 id="177" title="お問い合わせ"]'); ?>
        </div>
    </section>

    <?php get_template_part('template-parts/intern') ?>
</main>

<?php get_footer(); ?>