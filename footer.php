<?php
if (WP_DEBUG && current_user_can('manage_options')) {
    get_template_part('debug/config-data');
}

wp_footer();
?>
<?php $theme_options = get_option('theme_option_name'); ?>


<footer>
    <div class="page-width">

        <div class="footer-area">
            <div class="footer-area__corporate">
                <p class="footer-area__corporate--title">CORPORATE</p>
                <ul class="footer-area__corporate--links">
                    <li><a href="<?= home_url() ?>/about">キキョウファーマシーグループについて</a></li>
                    <li><a href="<?= home_url() ?>/service">事業内容</a></li>
                    <li><a href="<?= home_url() ?>/pharmacy">店舗一覧</a></li>
                    <li><a href="<?= home_url() ?>/news">お知らせ</a></li>
                    <li><a href="<?= home_url() ?>/contact">お問い合わせ</a></li>
                </ul>
            </div>
            <div class="footer-area__corporate">
                <p class="footer-area__corporate--title">RECRUIT</p>
                <ul class="footer-area__corporate--links">
                    <li><a href="<?= home_url() ?>/voice">代表メッセージと社員インタビュー</a></li>
                    <li><a href="<?= home_url() ?>/career">教育・キャリアプラン</a></li>
                    <li><a href="<?= home_url() ?>/info">数字で見る</a></li>
                    <li><a href="<?= home_url() ?>/oneday">1日の流れ</a></li>
                    <li><a href="<?= home_url() ?>/ict">ICTの活用</a></li>
                    <li><a href="<?= home_url() ?>/recruit">募集要項</a></li>
                    <li><a href="<?= home_url() ?>/faq">よくある質問</a></li>
                </ul>
            </div>
            <div class="footer-area__buttons">
                <!-- <a href="##">
                    職場見学予約<?php get_template_part('/images/svg/link-k-icon'); ?>
                </a> -->
                <a href="<?= home_url() ?>/entry">エントリー</a>
            </div>
        </div>
        <div class="footer-nav">
            <p><img src="<?= get_template_directory_uri() ?>/images/footer/logo.svg" alt="フッターロゴ"></p>
            <p class="footer-nav__info">TEL <a href="tel:<?php echo $theme_options['op_1']; ?>"><?php echo $theme_options['op_1']; ?></a></p>
            <p class="footer-nav__info">受付時間 <?php echo $theme_options['op_3']; ?></p>
            <p class="footer-nav__info">定休日 <?php echo $theme_options['op_4']; ?></p>

            <div class="footer-nav__buttons">
                <a href="http://kikyoyama.sub.jp/" class="footer-test" target="_blank">山本薬品<?php get_template_part('/images/svg/link-k-icon'); ?></a>
                <a class="ml comingsoon" href="##" target="_blank">ホワイトパス<?php get_template_part('/images/svg/link-k-icon'); ?></a>
                <a href="https://lieve.jp/" target="_blank">リヴェ<?php get_template_part('/images/svg/link-k-icon'); ?></a>
            </div>
        </div>
    </div>
    <div class="footer-copy">
        <div class="page-width">
            <div id="page-top">
                <?php get_template_part('/images/svg/page-top') ?>
            </div>
            <p class="copy">©︎2021 キキョウファーマシーグループ</p>
        </div>
    </div>

</footer>

</div>

</body>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script> -->
<script>
    $(function() {
        var pagetop = $('#page-top');
        pagetop.click(function() {
            $('body, html').animate({
                scrollTop: 0
            }, 1);
            return false;
        });
    });

    // リンクにホバーした時にクラス追加、離れたらクラス削除
    var link = document.querySelectorAll('a');
    for (var i = 0; i < link.length; i++) {
        link[i].addEventListener('mouseover', function(e) {
            cursor.classList.add('cursor--hover');
        });
        link[i].addEventListener('mouseout', function(e) {
            cursor.classList.remove('cursor--hover');
        });
    }

    $(function() {
        $('.comingsoon').on('click', function() {
            alert('coming soon...');
            return false;
        })
    })
</script>

</html>